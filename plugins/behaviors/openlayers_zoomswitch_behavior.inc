<?php

/**
 * @file
 * Implementation of OpenLayers behaviors
 */

/**
 * Ctools plugin definition.
 */
function openlayers_zoomswitch_openlayers_zoomswitch_behavior_openlayers_behaviors() {
  return array(
    'title' => t('Switch on zoom depth'),
    'description' => t('Switch layers based on the zoom depth.'),
    'type' => 'layer',
    'behavior' => array(
      'file' => 'openlayers_zoomswitch_behavior.inc',
      'class' => 'openlayers_zoomswitch_behavior',
      'parent' => 'openlayers_behavior',
    ),
  );
}

/**
 * Attribution Behavior
 */
class openlayers_zoomswitch_behavior extends openlayers_behavior {
  /**
   * Provide initial values for options.
   */
  function options_init() {
    return array(
      'zoomed' => array()
    ) + parent::options_init();
  }

  function options_form($defaults = array()) {
    $form = parent::options_form(array());

    // Only prompt for vector layers.
    $vector_layers = array();
    foreach ($this->map['layers'] as $id => $name) {
      $layer = openlayers_layer_load($id);
      if (isset($layer->data['vector']) && $layer->data['vector'] == TRUE) {
        $vector_layers[$id] = $name;
      }
    }

    $zoomlevels = array();
    for($i=20; $i>=0; $i--) {
      $zoomlevels[$i] = $i;
    }

    foreach ($vector_layers as $id => $name) {

      $layer = openlayers_layer_load($id);

      $form['zoomed'][$id] = array(
        '#type' => 'fieldset',
        '#title' => t('Options for layer @layer', array('@layer' => $name)),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
      );
      $form['zoomed'][$id]['enable'] = array(
        '#type' => 'checkbox',
        '#title' => t('Enable for this layer'),
        '#default_value' => !empty($defaults['zoomed'][$id]['enable']) ? 1 : FALSE,
      );
      // TODO
      //$form['zoomed'][$id]['disable'] = array(
      //  '#type' => 'checkbox',
      //  '#title' => t('Disable all other layers'),
      //  '#description' => t('Disable all the other layers when this layer gets active'),
      //  '#default_value' => !empty($defaults['zoomed'][$id]['disable']) ? 1 : FALSE,
      //);
      $form['zoomed'][$id]['visible'] = array(
        '#type' => 'select',
        '#title' => t('Visible from'),
        '#options' => $zoomlevels,
        '#description' => t('From what zoomlevel should this layer be active.'),
        '#default_value' => !empty($defaults['zoomed'][$id]['visible']) ? $defaults['zoomed'][$id]['visible'] : FALSE,
      );
      $form['zoomed'][$id]['hide'] = array(
        '#type' => 'select',
        '#title' => t('hide from'),
        '#options' => $zoomlevels,
        '#description' => t('Hide when zoomlevel get outside this range.'),
        '#default_value' => !empty($defaults['zoomed'][$id]['hide']) ? $defaults['zoomed'][$id]['hide'] : FALSE,
      );
    }
    return $form;
  }

  /**
   * Render.
   */
  function render(&$map) {
    drupal_add_js(drupal_get_path('module', 'openlayers_zoomswitch')
      . '/plugins/behaviors/openlayers_zoomswitch_behavior.js');
    return $this->options;
  }
}