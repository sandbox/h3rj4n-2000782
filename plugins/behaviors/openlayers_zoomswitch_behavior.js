/**
 * @file
 * OpenLayers Behavior implementation.
 */

Drupal.openlayers.addBehavior('openlayers_zoomswitch_behavior', function (context, options) {
  var map = context.openlayers;
  var triggers = new Array();

  var usesHeatmap = false;

  // Use a time out to be sure everything is loaded first.
  setTimeout(function() {
    // Detect if heatmap is used
    for (var i in map.layers) {
      if (map.layers[i] instanceof OpenLayers.Layer.Heatmap) {
        usesHeatmap = true;
      }
    }
  }, 100);

  // An extra variable to track the visibility of the heatmap.
  var heatmap_visible = true;
  var init_visible = new Array();

  map.events.register('zoomend', map, function() {
    var cur = map.getZoom();

    for (var i in options.zoomed) {
      layeroptions = options.zoomed[i];

      if (layeroptions.enable == 1) {
        if (layeroptions.visible <= cur
            && layeroptions.hide > cur) {
          // display layer.
          var layers = map.getLayersBy('drupalID', i);
          if (typeof layers != 'undefined') {
            var layer = layers[0];
            if (typeof init_visible[i] == 'undefined') {
              init_visible[i] = layer.getVisibility();
            }
            layer.setVisibility(!init_visible[i]);
          }

          // disable heatmap
          var layers = map.getLayersByName('heatmap');
          if (typeof layers != 'undefined') {
            var layer = layers[0];
            if (heatmap_visible) {
              layer.toggle();
              heatmap_visible = false;
            }
          }
        }
        else if (layeroptions.visible > cur
                 || layeroptions.hide < cur) {
          // Hide the layer.
          var layers = map.getLayersBy('drupalID', i);
          if (typeof layers != 'undefined') {
            var layer = layers[0];
            if (typeof init_visible[i] == 'undefined') {
              init_visible[i] = layer.getVisibility();
            }
            layer.setVisibility(init_visible[i]);
          }

          // Enable heatmap
          var layers = map.getLayersByName('heatmap');
          if (typeof layers != 'undefined') {
            var layer = layers[0];
            if (!heatmap_visible) {
              layer.toggle();
              heatmap_visible = true;
            }
          }
        }
      }
    }

  });
});